#!/usr/bin/env python
'''
The Lady Vanishes is basically hangman, but less overtly morbid. The game has
two players. One player chooses a word, and the other player's goal is to guess
the word by guessing one letter at a time. On the game board is a picture of a
woman (stick figure, use your imagination). She's wearing a hat that is the
letter `m`.  For each letter guessed that does not appear in the the word, the
first player chooses a part of the woman to disappear (i.e. turn into a space).
The game ends when the word is guessed correctly or the lady (including her hat)
vanishes entirely.

Usage:
./the_lady_vanishes.py <word>

Options:
-h --help         Print out this help message
'''
from docopt import docopt
from string import ascii_uppercase

art = r'''
    m
    O
   /|\
    |
   / \

Word: {dashes}

Last letter guessed:

Letters remaining:
{letters}
'''

if __name__ == '__main__' :

  opts = docopt(__doc__)

  art_d = {
    'dashes': ' '.join('_'*len(opts['<word>']))
    ,'letters': ' '.join(ascii_uppercase)
  }

  print(art.format(**art_d))
