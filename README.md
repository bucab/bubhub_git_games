# `git games` #

This repo contains [gamified](https://en.wikipedia.org/wiki/Gamification) learning materials for using [git](https://git-scm.com/).
It is a series of turn-based games that can be played with two or more people using `git push` and `git pull` as the mechanism
for making moves. The games implemented so far are:

* [NxM tic tac toe](https://en.wikipedia.org/wiki/Tic-tac-toe#Variations)
* [Dots and Boxes](https://en.wikipedia.org/wiki/Dots_and_Boxes)
* [The Lady Vanishes](https://en.wikipedia.org/wiki/Hangman_(game)) (basically the same thing as hangman, but less morbid)

To play, [fork this repo](https://bitbucket.org/bubioinformaticshub/bubhub_git_games/fork) and then invite someone to play with
by adding their bitbucket username in `Settings -> User and group access` with write permissions. Then both you and the other player
should clone your forked repo into your own working directories. Instructions and templates for each game are found within the
named subdirectories of the repo.

These games are designed to give players experience with four `git` commands: `git add`, `git commit`, `git push`, and `git pull`. Each game directory has a python script and an example file `board.txt` that contains a text representation of the corresponding game. Gameplay goes as follows:

  1. The first player edits the `board.txt` file with their move in their working copy
  1. The player then adds this move (file change) to the repo with `git add board.txt`
  1. The player then commits the change to the board with `git commit -m 'some possibly snarky commit message'`
  1. The player finally pushes the move back to bitbucket with `git push`
  1. When the push is complete, the second player runs `git pull` to see the first player's move in their own working copy
  1. The second player then performs steps 1-4 appropriately with their own move
  1. The first player performs a `git pull`, and the cycle repeats until the game is over

This is an honor system, people.
