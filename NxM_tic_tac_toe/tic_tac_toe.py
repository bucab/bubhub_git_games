#!/usr/bin/env python
'''
Creates a [NxM tic tac toe](https://en.wikipedia.org/wiki/Tic-tac-toe#Variations)
game instance with the requested number of rows and columns. Prints out the
board to standard output.

Usage:
./tic_tac_toe.py <rows> <cols>

Options:
-h --help      Print this help message
'''
from docopt import docopt

if __name__ == '__main__' :

  opts = docopt(__doc__)

  lines = []
  for row in range(int(opts['<rows>'])) :
    lines.append('|'.join(' '*int(opts['<cols>'])))
    lines.append('+'.join('-'*int(opts['<cols>'])))

  print('\n'.join(lines[:-1]))
