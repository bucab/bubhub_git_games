# NxM Tic Tac Toe

[NxM tic tac toe](https://en.wikipedia.org/wiki/Tic-tac-toe#Variations) is a
variation of the traditional 3x3 game extended to arbitrary dimensions. The
board game is a grid of empty cells, in this case 4x4:

```
 | | | 
-+-+-+-
 | | | 
-+-+-+-
 | | | 
-+-+-+-
 | | | 
```

Two players alternate putting an *X* or an *O* in any empty cell. The goal of
the game is to place a symbol that completes a row, column, or diagonal of
length `min(N,M)` of their own symbol. For example, the first player might play
as follows:

```
 | | | 
-+-+-+-
 | | | 
-+-+-+-
X| | | 
-+-+-+-
 | | | 
```

Followed by the second player:

```
 | | |O
-+-+-+-
 | | | 
-+-+-+-
X| | | 
-+-+-+-
 | | | 
```

And so on:

```
 | | |O
-+-+-+-
 | | | 
-+-+-+-
X|X| | 
-+-+-+-
 | | | 
```

```
 | | |O
-+-+-+-
 | | | 
-+-+-+-
X|X| |O
-+-+-+-
 | | | 
```

```
 | | |O
-+-+-+-
 | | | 
-+-+-+-
X|X| |O
-+-+-+-
 | | |X
```

Etc. Through a series of valient and dashingly handsome plays, player *X*
manages to have a play on the board as follows:

```
X| |O|O
-+-+-+-
 |X|O| 
-+-+-+-
X|X| |O
-+-+-+-
 | |O|X
```

Keenly noticing that victory is in sight along the downward left diagonal,
player *X* seals the deal and wins the game:

```
X| |O|O
-+-+-+-
 |X|O| 
-+-+-+-
X|X|X|O
-+-+-+-
 | |O|X
```

As with all tic tac toe games, the advantage is in the first player's favor,
which makes this game not very fun in general. But, it's a good example game
to use for git instructional purposes, so we'll just leave it in here.
