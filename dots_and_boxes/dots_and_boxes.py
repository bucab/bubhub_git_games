#!/usr/bin/env python
'''
Creates a [Dots and Boxes](https://en.wikipedia.org/wiki/Dots_and_Boxes)
game instance with the requested number of rows and columns. Prints out the
board to standard output.

Usage:
./dots_and_boxes.py <rows> <cols>

Options:
-h --help      Print this help message
'''
from docopt import docopt

if __name__ == '__main__' :

  opts = docopt(__doc__)

  lines = []
  for row in range(int(opts['<rows>'])) :
    lines.append(' '.join('.'*int(opts['<cols>'])))
    lines.append(' '.join(' '*int(opts['<cols>'])))

  print('\n'.join(lines[:-1]))
